package rede;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Vector;

import engine.Vars;
import objects.Vponto;

public class Rede implements Runnable {
	ObjectOutputStream saida;
	ObjectInputStream entrada;
	Socket conexao;
	String endereco;
	int porta;

	public Rede(String endereco, int porta) {
		this.endereco = endereco;
		this.porta = porta;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void run() {
		try

		{
			conexao = new Socket(endereco, porta);
			System.out.println("Conectado ao servidor " + endereco + ", na porta: " + porta);

			

			// lendo a mensagem enviada pelo servidor
			while (true) {
				// ligando as conexoes de saida e de entrada
				saida = new ObjectOutputStream(conexao.getOutputStream());
				saida.flush();
				entrada = new ObjectInputStream(conexao.getInputStream());
				Vars.pontos = (ArrayList<Vponto>) entrada.readObject();
				//System.out.println(Vars.pontos.size());
				
				saida.writeObject((Integer)Vars.getPlayerX());
				saida.writeObject((Integer)Vars.getPlayerY());
				saida.flush();
				// lendo a mensagem enviada pelo servidor

				if (Vars.getVidaAtual() > 200) {
					saida.close();
					entrada.close();
					conexao.close();
				}
				Thread.sleep(15);
			}
		} catch (Exception e) {
			System.err.println("erro: " + e.toString());
		}

	}

}
