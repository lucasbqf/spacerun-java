package arquivos;

import java.io.*;

import engine.Parametros;
/**
	cria opcoes para o jogo, estas opcoes sao salvas no c:/configs, nao existir nenhum arquivo, o jogo cria um arquivo e coloca as definioes
	padroes lah.
*/
public class ConfigHandler {
	
	public void criarConfig() {
		boolean success = (new File("C:/config/")).mkdirs();
		if (success) {
			File config = new File("C:/config/config.txt");
			//FileWriter configWriter = new FileWriter("../config/config.txt");
			try {
				Writer output = new BufferedWriter(new FileWriter(config));
				output.write(Parametros.tudo());
				output.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (!success) {
			File config = new File("C:/config/config.txt");
			try {
				BufferedReader input = new BufferedReader(new FileReader(config));
				Parametros.x=Integer.parseInt(input.readLine());
				Parametros.y=Integer.parseInt(input.readLine());
				Parametros.dificuldade=Integer.parseInt(input.readLine());
				Parametros.velocidadeAsteroide=Integer.parseInt(input.readLine());
				Parametros.Heat=Integer.parseInt(input.readLine());
				Parametros.raioPlayer=Integer.parseInt(input.readLine());
				Parametros.vidaPlayer=Integer.parseInt(input.readLine());
				input.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
	}
	
	public void leitor(String path) throws IOException {
		BufferedReader buffRead = new BufferedReader(new FileReader("C:\\"));
		String linha = "";
		while (true) {
			if (linha != null) {
				System.out.println(linha);
			} else
				break;
			linha = buffRead.readLine();
		}
		buffRead.close();
	}

	public static void escritor(String path) throws IOException {
		BufferedWriter buffWrite = new BufferedWriter(new FileWriter(path));
		String linha = "";
		//Scanner in = new Scanner(System.in);
		System.out.println("Escreva algo: ");
		//linha = in.nextLine();
		buffWrite.append(linha + "\n");
		buffWrite.close();
	}
}
