package tela;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import engine.Parametros;
import engine.Vars;

public class MenuPrincial extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	JButton estatisticas, novoJogo, opcoes, sair;
	JLabel spaceRun;
	boolean jogar = true;

	public MenuPrincial() {
		this.setLayout(null);
		this.setSize(Parametros.x, Parametros.y);
		super.setBackground(Color.black);

		spaceRun = new JLabel();
		try {
			Image spaceRunImg = ImageIO.read(getClass().getResource("../assets/SpaceRun.png"));
			spaceRunImg.getScaledInstance(100, 50, Image.SCALE_DEFAULT);
			spaceRun.setIcon(new ImageIcon(spaceRunImg));

		} catch (IOException ex) {
		}
		spaceRun.setOpaque(false);
		spaceRun.setSize(750, 200);
		spaceRun.setBounds(300, 0, spaceRun.getWidth(), spaceRun.getHeight());

		novoJogo = new JButton();
		try {
			Image novoJogoImg = ImageIO.read(getClass().getResource("../assets/novojogo.png"));
			novoJogoImg.getScaledInstance(100, 50, Image.SCALE_DEFAULT);
			novoJogo.setIcon(new ImageIcon(novoJogoImg));

		} catch (IOException ex) {
		}
		novoJogo.setOpaque(false);
		novoJogo.setSize(600, 200);
		novoJogo.setOpaque(false);
		novoJogo.setContentAreaFilled(false);
		novoJogo.setBorderPainted(false);
		novoJogo.setBounds(370, 150, novoJogo.getWidth(), novoJogo.getHeight());

		estatisticas = new JButton();
		try {
			Image estatisticasImg = ImageIO.read(getClass().getResource("../assets/estatisticas.png"));
			estatisticas.setIcon(new ImageIcon(estatisticasImg));
			estatisticasImg.getScaledInstance(100, 200, Image.SCALE_DEFAULT);
		} catch (IOException ex) {
		}

		estatisticas.setOpaque(false);
		estatisticas.setSize(700, 200);
		estatisticas.setContentAreaFilled(false);
		estatisticas.setBorderPainted(false);
		estatisticas.setBounds(320, 270, estatisticas.getWidth(), estatisticas.getHeight());

		opcoes = new JButton();
		try {
			Image opcoesImg = ImageIO.read(getClass().getResource("../assets/opcoes.png"));
			opcoes.setIcon(new ImageIcon(opcoesImg));
			opcoesImg.getScaledInstance(100, 200, Image.SCALE_DEFAULT);
		} catch (IOException ex) {
		}

		opcoes.setOpaque(false);
		opcoes.setSize(400, 200);
		opcoes.setContentAreaFilled(false);
		opcoes.setBorderPainted(false);
		opcoes.setBounds(450, 400, opcoes.getWidth(), opcoes.getHeight());

		sair = new JButton();
		try {
			Image sairImg = ImageIO.read(getClass().getResource("../assets/Sair.png"));
			sair.setIcon(new ImageIcon(sairImg));
			sairImg.getScaledInstance(100, 200, Image.SCALE_DEFAULT);
		} catch (IOException ex) {
		}

		sair.setOpaque(false);
		sair.setSize(250, 200);
		sair.setContentAreaFilled(false);
		sair.setBorderPainted(false);
		sair.setBounds(520, 520, sair.getWidth(), sair.getHeight());

		jogar = false;

		this.add(spaceRun);
		this.add(novoJogo);
		this.add(estatisticas);
		this.add(opcoes);
		this.add(sair);

		AcaoNovoJogo acaoNovoJogo = new AcaoNovoJogo();
		novoJogo.addActionListener(acaoNovoJogo);

		AcaoEstatistica acaoEstatistica = new AcaoEstatistica();
		estatisticas.addActionListener(acaoEstatistica);

		AcaoOpcoes acaoOpcoes = new AcaoOpcoes();
		opcoes.addActionListener(acaoOpcoes);

		AcaoSair acaoSair = new AcaoSair();
		sair.addActionListener(acaoSair);
	}

	public void paintComponent(Graphics g) {

		this.repaint();
	}

	public boolean isJogar() {
		return jogar;
	}

	public void setJogar(boolean jogar) {
		this.jogar = jogar;
	}

	class AcaoNovoJogo implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			Vars.setNovoJogo(true);
			System.out.println("novoJogo");
		}

	}

	class AcaoEstatistica implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			System.out.println("estatistica");
		}

	}

	class AcaoOpcoes implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			System.out.println("opcoes");
		}
	}

	class AcaoSair implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			System.out.println("sair");
			System.exit(0);
		}

	}

}
