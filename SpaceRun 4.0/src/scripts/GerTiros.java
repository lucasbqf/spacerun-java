package scripts;

import objects.PlayerShot;

public class GerTiros {
	public PlayerShot geraTiros(int vector2[]){
		PlayerShot tiro = new PlayerShot();
		tiro.setVector2(vector2[0], vector2[1]-10);
		tiro.setVelocidade(15);
		tiro.setRaioColisao(2);
		return tiro;
	}
}
