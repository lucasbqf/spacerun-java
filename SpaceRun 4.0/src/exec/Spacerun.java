package exec;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Label;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;

import javax.imageio.ImageIO;
import javax.swing.JLabel;
import javax.swing.JProgressBar;
import javax.swing.border.Border;

import arquivos.ConfigHandler;
import engine.Engine;
import engine.Parametros;
import engine.Vars;
//import engine.Parametros;
import scripts.OuvinteMouseJogo;
import scripts.SoundPlayer;
import tela.GameScreen;
import tela.MenuPrincial;
import tela.Painel;

@SuppressWarnings("unused")
public class Spacerun {

	public static void main(String[] args) {
		// objetos do jogo
		ConfigHandler config =new ConfigHandler();
		config.criarConfig();
		GameScreen telaJogo;
		Painel desenhosjogo, estatisticas;
		JProgressBar lifeBar;
		JLabel score, record;
		MenuPrincial menuPrincipal;

		telaJogo = new GameScreen();
		telaJogo.setResizable(false);
		telaJogo.setLayout(null);
		telaJogo.setBackground(Color.black);

		OuvinteMouseJogo ouvinte = new OuvinteMouseJogo();

		desenhosjogo = new Painel();
		desenhosjogo.setSize(Parametros.x, Parametros.y);

		lifeBar = new JProgressBar(0, Parametros.vidaPlayer);
		lifeBar.setStringPainted(true);
		lifeBar.setForeground(Color.red);
		lifeBar.setString("VIDA");
		lifeBar.setSize(130, 30);
		lifeBar.setOpaque(false);

		score = new JLabel();
		score.setSize(200, 20);
		score.setForeground(Color.WHITE);

		record = new JLabel();
		record.setSize(200, 20);
		record.setForeground(Color.WHITE);

		Insets tamanho = telaJogo.getInsets();

		menuPrincipal = new MenuPrincial();
		telaJogo.add(menuPrincipal);

		lifeBar.setBounds(10, 10, lifeBar.getWidth(), lifeBar.getHeight());
		score.setBounds(150, 10, score.getWidth(), score.getHeight());
		record.setBounds(250, 10, record.getWidth(), record.getHeight());

		desenhosjogo.addMouseListener(ouvinte);
		desenhosjogo.addMouseMotionListener(ouvinte);

		// Transparent 16 x 16 pixel cursor image.
		BufferedImage cursorImg = new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB);

		// Create a new blank cursor.
		Cursor blankCursor = Toolkit.getDefaultToolkit().createCustomCursor(cursorImg, new Point(0, 0), "blank cursor");
		Cursor dot = Toolkit.getDefaultToolkit().createCustomCursor(cursorImg, new Point(0, 0), "dot cursor");
		// la�o eterno do jogo
		while (true) {
			telaJogo.getContentPane().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
			//SoundPlayer musica = new SoundPlayer();
			//musica.playsound();
			if (Vars.isNovoJogo()) {
				
				telaJogo.remove(menuPrincipal);
				telaJogo.add(score);
				telaJogo.add(record);
				telaJogo.add(lifeBar);
				telaJogo.add(desenhosjogo);

				Engine motor = new Engine();
				telaJogo.getContentPane().setCursor(blankCursor);
				telaJogo.repaint();
				while (motor.getPlayerLife() >= 1) {

					// objeto no array de desenhos
					motor.Update();

					// atualiza as posicoes aonde estao cada objeto
					desenhosjogo.RefreshObj(motor.pontosobj);

					lifeBar.setValue(Vars.getVidaAtual());
					score.setText("pontos: " + Vars.getScore());
					record.setText("recorde: ");

					// redesenha toda a tela com todas as posicoes atualizadas
					telaJogo.repaint();

					// funcao para parar o jogo por 15 milisegundos
					try {
						Thread.sleep(15);
					} catch (InterruptedException ex) {
						Thread.currentThread().interrupt();
					}

				}
				telaJogo.remove(score);
				telaJogo.remove(record);
				telaJogo.remove(lifeBar);
				telaJogo.remove(desenhosjogo);
				telaJogo.add(menuPrincipal);
				Vars.setNovoJogo(false);

			}
			telaJogo.repaint();
			try {
				Thread.sleep(15);
			} catch (InterruptedException ex) {
				Thread.currentThread().interrupt();
			}
		}

	}

}
