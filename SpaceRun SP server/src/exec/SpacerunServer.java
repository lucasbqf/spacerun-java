package exec;

import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import engine.Engine;
import engine.Parametros;
import engine.Vars;
import objects.Vponto;
import rede.Rede;
//import engine.Parametros;

//@SuppressWarnings("unused")
public class SpacerunServer{

	public static void main(String[] args) {
		ArrayList<Vponto> ponto = new ArrayList<>();
		Vars.pontos = ponto;
		
		//System.out.println(Vars.pontos.toString());
		
		Rede rede = new Rede(Parametros.porta);
		ExecutorService GerenciaThread = Executors.newFixedThreadPool(2);
		GerenciaThread.execute(rede);
		// la�o eterno do jogo
		while (true) {
			// if (Vars.isNovoJogo()) {
			Engine motor = new Engine();
			while (motor.getPlayerLife() >= 1) {

				// objeto no array de desenhos
				motor.Update();

				// atualiza as posicoes aonde estao cada objeto
				//Vars.pontos = motor.pontosobj;
				//System.out.println(Vars.pontos.toString());
				// funcao para parar o jogo por 15 milisegundos
				try {
					Thread.sleep(15);
				} catch (InterruptedException ex) {
					Thread.currentThread().interrupt();
				}

			}

			// }
			try {
				Thread.sleep(15);
			} catch (InterruptedException ex) {
				Thread.currentThread().interrupt();
			}
		}

	}

}
