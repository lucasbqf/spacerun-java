package objects;

import engine.Parametros;

public class Player extends Obj {
	private int vida;
	
	public Player(){
		setVida(Parametros.vidaPlayer);
		setRaioColisao(Parametros.raioPlayer);
	}
	public int getVida() {
		return vida;
	}
	public void setVida(int vida) {
		this.vida = vida;
	}
}
