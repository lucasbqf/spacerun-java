package scripts;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import engine.Vars;

public class OuvinteMouseJogo implements MouseListener, MouseMotionListener {
	private boolean atualizar = true;

	private void setVars(MouseEvent e) {
		Vars.setPlayerX(e.getX());
		Vars.setPlayerY(e.getY());
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		if (atualizar)
			setVars(e);
		Vars.setAtirou(true);

	}

	@Override
	public void mouseMoved(MouseEvent e) {
		if (atualizar)
			setVars(e);
		Vars.setAtirou(false);
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		Vars.setAtirou(true);
	}

	@Override
	public void mousePressed(MouseEvent e) {
		Vars.setAtirou(true);
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		Vars.setAtirou(false);

	}

	@Override
	public void mouseEntered(MouseEvent e) {
		Vars.setAtirou(false);
		atualizar = true;
	}

	@Override
	public void mouseExited(MouseEvent e) {
		Vars.setAtirou(false);
		atualizar = false;

	}

}