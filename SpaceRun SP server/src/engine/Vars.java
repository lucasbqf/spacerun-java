package engine;

import java.util.ArrayList;

import objects.Player;
import objects.Vponto;

public class Vars {
	static private boolean novoJogo = false, jogar = true, atirou = false;
	static public int playerX, playerY;
	static private int vidaAtual;
	static private int score;
	public static ArrayList<Vponto> pontos;
	public static ArrayList<Integer> posicaoJogador;

	public Vars(){
		pontos = new ArrayList<>();
	}
	public static boolean isNovoJogo() {
		return novoJogo;
	}

	public static void setNovoJogo(boolean novoJogo) {
		Vars.novoJogo = novoJogo;
	}

	public static boolean isJogar() {
		return jogar;
	}

	public static void setJogar(boolean jogar) {
		Vars.jogar = jogar;
	}

	public static int getPlayerX() {
		return playerX;
	}

	public static void setPlayerX(int playerX) {
		Vars.playerX = playerX;
	}

	public static int getPlayerY() {
		return playerY;
	}

	public static void setPlayerY(int playerY) {
		Vars.playerY = playerY;
	}

	public static int getVidaAtual() {
		return vidaAtual;
	}

	public static void setVidaAtual(int vidaAtual) {
		Vars.vidaAtual = vidaAtual;
	}

	public static int getScore() {
		return score;
	}

	public static void setScore(int score) {
		Vars.score = score;
	}

	public static boolean isAtirou() {
		return atirou;
	}

	public static void setAtirou(boolean atirou) {
		Vars.atirou = atirou;
	}

}
