package rede;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

import engine.Vars;
import objects.Player;
import objects.Vponto;

public class Rede implements Runnable {
	ObjectInputStream entrada;
	ObjectOutputStream saida;
	boolean sair = false;
	int porta;

	public Rede(int porta) {
		this.porta = porta;
	}
	@Override
	public void run() {
		ServerSocket servidor;
		try {
			servidor = new ServerSocket(porta, 3);
			Socket conexao;
			System.out.println("escutando a Porta: "+porta);
			conexao = servidor.accept();
			while (!sair) {
				saida = new ObjectOutputStream(conexao.getOutputStream());
				entrada = new ObjectInputStream(conexao.getInputStream());
				ArrayList<Vponto> pontos = new ArrayList<>(Vars.pontos);
				saida.writeObject(pontos);
				//System.out.println(pontos.size());
				try {
					// obtendo a mensagem enviada pelo cliente
					ArrayList<Integer> posicao = new ArrayList<>();
					entrada.readObject();
					Vars.playerX=(Integer)entrada.readObject();
					Vars.playerY=(Integer)entrada.readObject();
				} catch (IOException iOException) {
					System.err.println("erro: " + iOException.toString());
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if (Vars.getVidaAtual() < -1) {
					sair = true;
					saida.close();
					entrada.close();
					conexao.close();
				}
				Thread.sleep(15);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
