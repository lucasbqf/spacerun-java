package arquivos;

import java.io.*;

import engine.Parametros;
/**
	cria opcoes para o jogo, estas opcoes sao salvas no c:/configs, nao existir nenhum arquivo, o jogo cria um arquivo e coloca as definioes
	padroes lah.
*/
public class ConfigHandler {
	
	public void criarConfig() {
		boolean success = (new File("C:/config/")).mkdirs();
		if (success) {
			File config = new File("C:/config/config.txt");
			//FileWriter configWriter = new FileWriter("../config/config.txt");
			try {
				Writer output = new BufferedWriter(new FileWriter(config));
				output.write(Parametros.tudo());
				output.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (!success) {
			File config = new File("C:/config/config.txt");
			try {
				BufferedReader input = new BufferedReader(new FileReader(config));
				input.readLine();
				Parametros.x=Integer.parseInt(input.readLine());
				input.readLine();
				Parametros.y=Integer.parseInt(input.readLine());
				input.readLine();
				Parametros.dificuldade=Integer.parseInt(input.readLine());
				input.readLine();
				Parametros.velocidadeAsteroide=Integer.parseInt(input.readLine());
				input.readLine();
				Parametros.Heat=Integer.parseInt(input.readLine());
				input.readLine();
				Parametros.raioPlayer=Integer.parseInt(input.readLine());
				input.readLine();
				Parametros.vidaPlayer=Integer.parseInt(input.readLine());
				input.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
	}
}
