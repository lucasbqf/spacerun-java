package Objetos;

public class PlayerShot extends Obj {
	private int velocidade;
	private int forca;
	private int JogadorOrigem;
	
	public PlayerShot (int mjogador, int x, int y ){
		this.setVector2(x, y);
		JogadorOrigem = mjogador;
	}
	public int getVelocidade() {
		return velocidade;
	}
	public void setVelocidade(int velocidade) {
		this.velocidade = velocidade;
	}
	public int getForca() {
		return forca;
	}
	public void setForca(int forca) {
		this.forca = forca;
	}
	public int getJogadorOrigem() {
		return JogadorOrigem;
	}
	public void setJogadorOrigem(int jogadorOrigem) {
		JogadorOrigem = jogadorOrigem;
	}
}
