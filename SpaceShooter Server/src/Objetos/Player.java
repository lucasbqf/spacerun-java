package Objetos;

import Motor.Parametros;

public class Player extends Obj {
	private int vida;
	private int numeroJogaror;
	private boolean atirou;
	
	public Player(){
		setVida(Parametros.vidaJogador);
		setRaioColisao(Parametros.raioJogador);
	}
	public int getVida() {
		return vida;
	}
	public void setVida(int vida) {
		this.vida = vida;
	}
	public boolean isAtirou() {
		return atirou;
	}
	public void setAtirou(boolean atirou) {
		this.atirou = atirou;
	}
	public int getNumeroJogaror() {
		return numeroJogaror;
	}
	public void setNumeroJogaror(int numeroJogaror) {
		this.numeroJogaror = numeroJogaror;
	}
}
