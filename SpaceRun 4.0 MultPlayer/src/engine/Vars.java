package engine;

import java.util.ArrayList;

import objects.Vponto;

public class Vars {
	 public static class PontoJogador{

		private int x=5;
		private int y=5;
		private int vida;
		private boolean atirou;

		public int getX() {
			return x;
		}

		public void setX(int x) {
			this.x = x;
		}

		public int getY() {
			return y;
		}
		
		public void setY(int y) {
			this.y = y;
		}

		public int getVida() {
			return vida;
		}

		public void setVida(int vida) {
			this.vida = vida;
		}

		public boolean isAtirou() {
			return atirou;
		}

		public void setAtirou(boolean atirou) {
			this.atirou = atirou;
		}
		
	}
	
	static private boolean novoJogo = false, jogar = true;
	static private int score;
	static public PontoJogador jogadores[]={new PontoJogador(),new PontoJogador()};
	static public ArrayList<Vponto> PontosDesenhos;
	static public int nPlayer;
	public Vars() {
	//jogadores[0]= new PontoJogador();
	//jogadores[1]= new PontoJogador();
	}
	
	
	public static boolean isNovoJogo() {
		return novoJogo;
	}

	public static void setNovoJogo(boolean novoJogo) {
		Vars.novoJogo = novoJogo;
	}

	public static boolean isJogar() {
		return jogar;
	}

	public static void setJogar(boolean jogar) {
		Vars.jogar = jogar;
	}

	public static int getPlayerX(int numeroPlayer) {
		return jogadores[numeroPlayer].getX();
	}

	public static void setPlayerX(int numeroPlayer,int playerX) {
		jogadores[numeroPlayer].setX(playerX);
	}

	public static int getPlayerY(int numeroPlayer) {
		return jogadores[numeroPlayer].getY();
	}

	public static void setPlayerY(int numeroPlayer,int playerY) {
		jogadores[numeroPlayer].setY(playerY);
	}

	public static int getVidaAtual(int numeroPlayer) {
		return jogadores[numeroPlayer].getVida();
	}

	public static void setVidaAtual(int numeroPlayer ,int vidaAtual) {
		jogadores[numeroPlayer].setVida(vidaAtual);
	}

	public static int getScore() {
		return score;
	}

	public static void setScore(int score) {
		Vars.score = score;
	}

	public static boolean isAtirou(int numeroPlayer) {
		return jogadores[numeroPlayer].isAtirou();
	}

	public static void setAtirou(int numeroPlayer,boolean atirou) {
		jogadores[numeroPlayer].setAtirou(atirou);
	}

}
