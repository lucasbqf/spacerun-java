package tela;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

import engine.Parametros;
import engine.Vars;
import objects.Vponto;

@SuppressWarnings("serial")
public class Painel extends JPanel {
	ArrayList<Vponto> asteroides = new ArrayList<>();
	public int jogadorX, jogadorY;
	Image player, ast1, ast2, ast3, ast4, ast5,ast6;

	public void setBackground(Color bg) {
		// TODO Auto-generated method stub
		super.setBackground(Color.black);

		try {
			player = ImageIO.read(getClass().getResource("../assets/nave.png"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			ast1 = ImageIO.read(getClass().getResource("../assets/ast1.png"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			ast2 = ImageIO.read(getClass().getResource("../assets/ast2.png"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			ast3 = ImageIO.read(getClass().getResource("../assets/ast3.png"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			ast4 = ImageIO.read(getClass().getResource("../assets/ast4.png"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			ast5 = ImageIO.read(getClass().getResource("../assets/ast5.png"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			ast6 = ImageIO.read(getClass().getResource("../assets/ast6.png"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void paintComponent(Graphics g) {
		jogadorX = Vars.jogadores[Vars.nPlayer].getX();
		jogadorY = Vars.jogadores[Vars.nPlayer].getX();
		// chama paintComponent para assegurar que o painel � exibido
		// corretamente
		super.paintComponent(g);
		g.setColor(Color.white);

		for (Vponto obj : asteroides) {
			if (obj.tipo == 'A') {
				if (obj.desenho == 1)
					g.drawImage(ast1, obj.X - obj.RaioColisao, obj.Y - obj.RaioColisao, obj.RaioColisao * 2,
							obj.RaioColisao * 2, null);
				if (obj.desenho == 2)
					g.drawImage(ast2, obj.X - obj.RaioColisao, obj.Y - obj.RaioColisao, obj.RaioColisao * 2,
							obj.RaioColisao * 2, null);
				if (obj.desenho == 3)
					g.drawImage(ast3, obj.X - obj.RaioColisao, obj.Y - obj.RaioColisao, obj.RaioColisao * 2,
							obj.RaioColisao * 2, null);
				if (obj.desenho == 4)
					g.drawImage(ast4, obj.X - obj.RaioColisao, obj.Y - obj.RaioColisao, obj.RaioColisao * 2,
							obj.RaioColisao * 2, null);
				if (obj.desenho == 5)
					g.drawImage(ast5, obj.X - obj.RaioColisao, obj.Y - obj.RaioColisao, obj.RaioColisao * 2,
							obj.RaioColisao * 2, null);
				if (obj.desenho == 6)
					g.drawImage(ast6, obj.X - obj.RaioColisao, obj.Y - obj.RaioColisao, obj.RaioColisao * 2,
							obj.RaioColisao * 2, null);
			} else if (obj.tipo == 'T') {
				g.drawOval(obj.X - obj.RaioColisao, obj.Y - obj.RaioColisao, obj.RaioColisao * 2, obj.RaioColisao * 2);
			}
		}

		g.drawImage(player, jogadorX - Parametros.raioPlayer, jogadorY - Parametros.raioPlayer,
				Parametros.raioPlayer * 2, Parametros.raioPlayer * 2, null);

	}

	public void RefreshObj(ArrayList<Vponto> array) {
		asteroides = array;
	}
}