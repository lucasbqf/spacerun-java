package tela;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import motor.Variaveis;

public class OuvinteMouseJogo implements MouseListener, MouseMotionListener {
	private boolean atualizar = true;

	private void setVars(MouseEvent e) {
		Variaveis.setPlayerX(e.getX());
		Variaveis.setPlayerY(e.getY());
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		if (atualizar)
			setVars(e);
		Variaveis.setAtirou(true);

	}

	@Override
	public void mouseMoved(MouseEvent e) {
		if (atualizar)
			setVars(e);
		Variaveis.setAtirou(false);
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		Variaveis.setAtirou(true);
	}

	@Override
	public void mousePressed(MouseEvent e) {
		Variaveis.setAtirou(true);
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		Variaveis.setAtirou(false);

	}

	@Override
	public void mouseEntered(MouseEvent e) {
		Variaveis.setAtirou(false);
		atualizar = true;
	}

	@Override
	public void mouseExited(MouseEvent e) {
		Variaveis.setAtirou(false);
		atualizar = false;

	}

}