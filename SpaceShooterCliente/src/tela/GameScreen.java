package tela;


import javax.swing.JFrame;

import motor.Parametros;

@SuppressWarnings("serial")
public class GameScreen extends JFrame {
	
	public GameScreen(){
		// Configura um t�tulo para a janela
		super("SpaceRun");
		// Determina a opera��o padr�o ao se fechar a janela
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		// Determina o tamanho da janela
		setSize(Parametros.x,Parametros.y);
		//setExtendedState(JFrame.MAXIMIZED_BOTH);
		// Torna a janela visivel
		this.setUndecorated(true);
		setVisible(true);
		 
		
		
	}
	
}
