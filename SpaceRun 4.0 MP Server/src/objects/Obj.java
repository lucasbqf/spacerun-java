package objects;
/**
 * classe abstrata em que todos os objetos em jogo devem ser derivados
 * contem metodos geometricos de posicao e movimento assim como o tempo de instanciacao do objeto 
 * @author lucas
 *
 */
public abstract class Obj {
	private int Vector2[]= {0,0};
	private float initTime = System.currentTimeMillis( );
	private int raioColisao;
	
	public int getRaioColisao() {
		return raioColisao;
	}
	public void setRaioColisao(int raioColisao) {
		this.raioColisao = raioColisao;
	}
	public void setRelativeToCurrentPos(int x,int y){
		Vector2[0] =Vector2[0] + x;
		Vector2[1] =Vector2[1] + y;
	}
	public int[] getVector2() {
		return Vector2;
	}

	public void setVector2(int x,int y) {
		Vector2[0] = x;
		Vector2[1] = y;
	}
	
	public int getVectorX() {
		return Vector2[0];
	}
	public int getVectorY() {
		return Vector2[1];
	}
	
	public float getInitTime() {
		return initTime;
	}
	@SuppressWarnings("unused")
	private void setInitTime(float initTime) {
		this.initTime = initTime;
	}
	public int DistEntrePontos(Obj obj){
		return (int) Math.sqrt(Math.pow(getVectorX()-obj.getVectorX(),2)+Math.pow(getVectorY()-obj.getVectorY(),2));

	}
	
	public boolean  colide(Obj obj1){
		int distancia;
		int SomaRaios;
		distancia=this.DistEntrePontos(obj1);
		SomaRaios=this.getRaioColisao()+obj1.getRaioColisao();
		if(distancia < SomaRaios){
			return true;
		}
		else return false;
	}
	public abstract String informacoes();
}
