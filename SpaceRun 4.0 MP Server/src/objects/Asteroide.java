package objects;

public class Asteroide extends Obj {
	int velocidade;
	int velocidadeLateral;
	int tipo;
	
	public int getTipo() {
		return tipo;
	}
	public void setTipo(int tipo) {
		this.tipo = tipo;
	}
	public int getVelocidadeLateral() {
		return velocidadeLateral;
	}
	public void setVelocidadeLateral(int velocidadeLateral) {
		this.velocidadeLateral = velocidadeLateral;
	}
	public int getVelocidade() {
		return velocidade;
	}
	public void setVelocidade(int velocidade) {
		this.velocidade = velocidade;
	}
	
	public String informacoes(){
		int[] vetor=getVector2();
		return "3:"+vetor[0]+":"+vetor[1]+":"+ getRaioColisao()+"\n";
	}
}
