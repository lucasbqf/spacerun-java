package objects;

public class PlayerShot extends Obj {
	private int velocidade;
	private int forca;
	public int getVelocidade() {
		return velocidade;
	}
	public void setVelocidade(int velocidade) {
		this.velocidade = velocidade;
	}
	public int getForca() {
		return forca;
	}
	public void setForca(int forca) {
		this.forca = forca;
	}
	@Override
	public String informacoes() {
		
		return "2:"+getVectorX()+":"+getVectorY()+":"+getVelocidade();
	}
}
