package objects;

import engine.Parametros;

public class Player extends Obj {
	private int vida;
	private int Heat;
	
	public Player(){
		setVida(Parametros.vidaPlayer);
		setRaioColisao(Parametros.raioPlayer);
	}
	public int getVida() {
		return vida;
	}
	public void setVida(int vida) {
		this.vida = vida;
	}
	public int getHeat() {
		return Heat;
	}
	public void setHeat(int heat) {
		Heat = heat;
	}
	@Override
	public String informacoes() {
		int[] vetor=getVector2();
		return "1:"+vetor[0]+":"+vetor[1]+":"+ getRaioColisao();
	}
}
