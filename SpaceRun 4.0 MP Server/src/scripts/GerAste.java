package scripts;

import objects.Obj;

import java.util.Random;

import objects.Asteroide;
/**
 * classe para a geracao dos asteroides, gerando em posicoes aleatorias em relacao ao eixo x
 * @author lucas
 *
 */
public class GerAste extends Obj {
	public Asteroide gerarAst(int vector2[],boolean lateral){
		Random rand = new Random();
			Asteroide ast = new Asteroide();
			ast.setVector2(vector2[0],vector2[1]);
			ast.setVelocidade(1+ rand.nextInt(5));
			if(lateral)
				ast.setVelocidadeLateral(rand.nextInt(6)-3);
			ast.setRaioColisao(20+(rand.nextInt(6)*5));
			ast.setTipo(rand.nextInt(5)+1);
			return ast; 
	}
	public String informacoes(){
		return null;
	}


}
