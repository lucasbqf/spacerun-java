package exec;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import engine.Engine;
import rede.ConexaoRedeEnvia;
import rede.ConexaoRedeRecebe;

public class Server {
	
	public static void main(String[] args) {

		Engine Motor = new Engine();
		ConexaoRedeRecebe RedeRecebe = new ConexaoRedeRecebe();
		ConexaoRedeEnvia redeEnvia = new ConexaoRedeEnvia();
		ExecutorService GerenciaThread = Executors.newFixedThreadPool( 3 );
		
		GerenciaThread.execute(Motor);
		//GerenciaThread.execute(RedeRecebe);
		GerenciaThread.execute(redeEnvia);
		
		
	}

	
	
	
	
	
	
	
	
	
	
	static void pause() {
		try {
			Thread.sleep(15);
		} catch (InterruptedException ex) {
			Thread.currentThread().interrupt();
		}
	}

}
