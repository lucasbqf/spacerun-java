package rede;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

import engine.Engine;
import engine.Parametros;

public class ConexaoRedeEnvia implements Runnable {
	private byte[] mensagemBytes; // dados da mensagem
	Mensagem mensagem;
	int contador;

	public ConexaoRedeEnvia() {
		contador = 0;
	}

	@Override
	public void run() {
		while (true) {
			contador++;
			try
			// entrega a mensagem
			{
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				ObjectOutputStream oos = new ObjectOutputStream(baos);
				mensagem = new Mensagem(Engine.pontosobj, contador);
				oos.writeObject(mensagem);
				oos.flush();
				mensagemBytes = baos.toByteArray();
				System.out.println("enviando mensagem "+ contador);
				System.out.println("tamanho array "+mensagem.getTamanho() );
				// cria o DatagramSocket para enviar a mensagem
				DatagramSocket socket = new DatagramSocket(Parametros.portaEnvio);

				// utiliza o InetAddress reservado para grupo de multicast
				InetAddress group = InetAddress.getByName(Parametros.enderecoMulticast);

				// cria o DatagramPacket contendo a mensagem
				DatagramPacket packet = new DatagramPacket(mensagemBytes, mensagemBytes.length, group,
						Parametros.portaRecebimento);

				socket.send(packet); // envia o pacote para o grupo de multicast
				socket.close(); // fecha o socket
			} // fim do try
			catch (IOException ioException) {
				ioException.printStackTrace();
				System.out.println("DEU PAU 1");
			} // fim do catch
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				System.out.println("DEU PAU 2");
				e.printStackTrace();
			}
		}
	}// fim do m�todo run
}
