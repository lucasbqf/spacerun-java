package rede;

import java.io.Serializable;
import java.util.ArrayList;

import objects.Vponto;
/**
 * classe que serializavel que contem os pontos dos objetos em jogo
 * @author lucas
 *
 */
public class Mensagem implements Serializable{
	private static final long serialVersionUID = 1L;
	private ArrayList<Vponto> pontos;
	private int contador;
	
	public Mensagem (ArrayList<Vponto> ponto,int contador){
		this.pontos=ponto;
		this.contador=contador;
	}
	public ArrayList<Vponto> getPontos(){
		return pontos;
	}
	public int getContador(){
		return this.contador;
	}
	public int getTamanho(){
		return pontos.size();
	}
}
