package rede;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

import engine.Parametros;

@SuppressWarnings("unused")
public class ConexaoRedeRecebe implements Runnable{
	@SuppressWarnings("resource")
	public void run() {
		DatagramSocket serverSocket = null;
		
		try {
			serverSocket = new DatagramSocket(Parametros.portaRecebimento);
			InetAddress	multicastGroup = InetAddress.getByName(Parametros.enderecoMulticast);
		} catch (SocketException e) {
			e.printStackTrace();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		byte[] receiveData = new byte[Parametros.tamanhoMensagem];
		while (true) {
			DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
			try {
				serverSocket.receive(receivePacket);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			String sentence = new String(receivePacket.getData());
			System.out.println("RECEIVED: " + sentence);
			
			
			
			
			/*
			InetAddress IPAddress = receivePacket.getAddress();
			int port = receivePacket.getPort();
			
			String capitalizedSentence = sentence.toUpperCase(); */
		}
	}
}
