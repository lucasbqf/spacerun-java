package engine;

import java.util.ArrayList;
import java.util.Random;

import objects.Asteroide;
import objects.Player;
import objects.PlayerShot;
import objects.Vponto;
import scripts.GerAste;
import scripts.GerTiros;
import engine.Vars;

public class Engine implements Runnable {

	// objetos em jogo
	final Player[] jogadores = { new Player(), new Player() };

	GerAste geradorAst = new GerAste();
	GerTiros geradorTiros = new GerTiros();
	StringBuffer saida;
	private ArrayList<Asteroide> objetos = new ArrayList<>();
	private ArrayList<PlayerShot> tiros = new ArrayList<>();
	static public ArrayList<Vponto> pontosobj = new ArrayList<>();

	int ciclos = 0;
	int contGerador = 0;
	int dificuldade = Parametros.dificuldade;
	// public int vidaPlayer = Parametros.vidaPlayer;
	public int pontuacao = 0;

	public Engine() {
		jogadores[0].setVida(Parametros.vidaPlayer);
		jogadores[1].setVida(Parametros.vidaPlayer);
		saida= new StringBuffer(" ");
	}

	// ________________________________________________________________________________________________
	// A magica acontece aqui!!!
	public void Update() {
		Vars.setScore(pontuacao);

		jogadores[0].setVector2(Vars.jogadores[0].getX(), Vars.jogadores[0].getY());
		jogadores[1].setVector2(Vars.jogadores[1].getX(), Vars.jogadores[1].getY());
		geraAsteroides();
		if (Vars.jogadores[0].isAtirou()) {
			geraTiros(0);
		}
		if (Vars.jogadores[1].isAtirou()) {
			geraTiros(1);
		}
		
		moveAsteroides();
		moveShots();
		TestesColisao();
		atualizapontos();
		
		// Vars.setVidaAtual(vidaPlayer);
		//debuger();
	
		//System.out.println(Vars.Saida +"\n\n\n\n\n\n\n\n");
		if (ciclos % 300 == 0 && dificuldade > 1) {
			dificuldade--;
		}
		ciclos++;
	}
	
	// ________________________________________________________________________________________________

	// debuger, metodo apenas para verificacao dos atributos em linha de comando
	void debuger() {
		int vector2P1[] = jogadores[0].getVector2();
		System.out.printf("Player X:%d", vector2P1[0]);
		System.out.printf(" | Y:%d \n", vector2P1[1]);
		System.out.printf(" vida P1:%d \n\n", jogadores[0].getVida());

		int vector2P2[] = jogadores[1].getVector2();
		System.out.printf("Player X:%d", vector2P2[0]);
		System.out.printf(" | Y:%d \n", vector2P2[1]);
		System.out.printf(" vida P2:%d \n\n", jogadores[1].getVida());

		System.out.printf("num Ast:%d \n", objetos.size());
		System.out.printf("dificuldade:%d \n", dificuldade);
		System.out.printf("num tiros:%d \n\n", tiros.size());
		System.out.flush();

	}

	// funcao que passa os tipos e as posicoes de todos os objetos que estao no
	// motor para ser visualizado pelo painel
	void atualizapontos() {
		pontosobj.clear();
		// passa os asteroides
		for (Asteroide obj : objetos) {
			int vectorA[] = obj.getVector2();
			Vponto pontoxy = new Vponto();
			pontoxy.X = vectorA[0];
			pontoxy.Y = vectorA[1];
			pontoxy.RaioColisao = obj.getRaioColisao();
			pontoxy.tipo = 'A';
			pontoxy.desenho = obj.getTipo();
			pontosobj.add(pontoxy);
			

		}
		// passa os tiros
		for (PlayerShot tiro : tiros) {
			int vectorA[] = tiro.getVector2();
			Vponto pontoxy = new Vponto();
			pontoxy.X = vectorA[0];
			pontoxy.Y = vectorA[1];
			pontoxy.tipo = 'T';
			pontoxy.RaioColisao = tiro.getRaioColisao();
			pontosobj.add(pontoxy);
			//saida.append(tiro.informacoes());
		}
		Vars.PontosDesenhos=pontosobj;
	}

	// gera asteroides em um pontos aleatorios em x
	void geraAsteroides() {
		if (contGerador > dificuldade) {
			Random rand = new Random();
			int vector2[] = { rand.nextInt(Parametros.x + 100) - 50, 0 };
			objetos.add(geradorAst.gerarAst(vector2, true));
			contGerador = 0;
			pontuacao = pontuacao + 1;
		} else
			contGerador++;
	}

	void geraTiros(int numeroPlayer) {
		if (jogadores[numeroPlayer].getHeat() > Parametros.Heat) {
			tiros.add(geradorTiros.geraTiros(jogadores[numeroPlayer].getVector2()));
			jogadores[numeroPlayer].setHeat(0);
		} else
			jogadores[numeroPlayer].setHeat(jogadores[numeroPlayer].getHeat() + 1);
	}

	// move os asteroides para qualquer posicao referente ao propio objeto
	void moveAsteroides() {
		int cont = 0;
		while (cont < objetos.size() && objetos.size() > 0) {
			int vector2[] = objetos.get(cont).getVector2();

			objetos.get(cont).setRelativeToCurrentPos(
					(1 * Parametros.velocidadeAsteroide) * objetos.get(cont).getVelocidadeLateral(),
					(1 * Parametros.velocidadeAsteroide) * objetos.get(cont).getVelocidade());
			if (vector2[1] >= Parametros.y + 50) {
				objetos.remove(objetos.get(cont));
				cont--;
			}
			cont++;
		}
	}

	// move os tiros e retira da tela se nao se chocarem com nada
	void moveShots() {
		int cont = 0;
		while (cont < tiros.size() && tiros.size() > 0) {
			tiros.get(cont).setRelativeToCurrentPos(0, (-1) * tiros.get(cont).getVelocidade());
			int vector2[] = tiros.get(cont).getVector2();
			if (vector2[1] <= 0) {
				tiros.remove(tiros.get(cont));
				cont--;
			}
			cont++;
		}
	}

	// testes de colisao entre os objetos
	void TestesColisao() {
		int contAst = 0;
		while (contAst < objetos.size() && objetos.size() > 0) {
			int contTiro = 0;
			if (objetos.get(contAst).colide(jogadores[0])) {
				jogadores[0].setVida(jogadores[0].getVida() - 1);
				// vidaPlayer = jogador.getVida();
				objetos.remove(objetos.get(contAst));

				if (contAst > 1)
					contAst--;
				else
					contAst = 0;
			}
			if (objetos.get(contAst).colide(jogadores[1])) {
				jogadores[1].setVida(jogadores[1].getVida() - 1);
				// vidaPlayer = jogador.getVida();
				objetos.remove(objetos.get(contAst));

				if (contAst > 1)
					contAst--;
				else
					contAst = 0;
			}
			while (contTiro < tiros.size() && tiros.size() > 0) {
				Random rand = new Random();
				// se o tiro colide com o asteroide e com vida maior q 10
				if (objetos.get(contAst).colide(tiros.get(contTiro)) && objetos.get(contAst).getRaioColisao() > 10) {
					tiros.remove(tiros.get(contTiro));
					objetos.get(contAst).setRaioColisao(objetos.get(contAst).getRaioColisao() - 10);
					objetos.get(contAst).setTipo(rand.nextInt(5) + 1);
					contTiro = 0;
					pontuacao = pontuacao + 10;
				}

				// se o tiro colide com o asteroide e com vida menor q 10
				else if (objetos.get(contAst).colide(tiros.get(contTiro))
						&& objetos.get(contAst).getRaioColisao() <= 10) {
					objetos.remove(objetos.get(contAst));
					tiros.remove(tiros.get(contTiro));
					pontuacao = pontuacao + 50;
					if (contAst > 1)
						contAst--;
					else
						contAst = 0;
					contTiro = 0;
				}
				contTiro++;
			}
			contAst++;
		}
	}

	public int getPlayerLife(int numeroPlayer) {
		return jogadores[numeroPlayer].getVida();
	}

	@Override
	public void run() {
		while (true) {
			Update();
			try {
				Thread.sleep(15);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
