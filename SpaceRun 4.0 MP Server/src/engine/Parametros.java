package engine;

public class Parametros {
	
	//parametros jogo
	static public int 	x= 1366;
	static public int	y= 768;
	static public int dificuldade = 15;
	static public float velocidadeJogador = 1;
	static public int	velocidadeAsteroide=1;
	static public int 	Heat = 10;
	static public int 	raioPlayer=15;
	static public int vidaPlayer=3;
	
	//parametro de redes
	static public int portaEnvio=8002;
	static public int portaRecebimento=8001;
	static public String enderecoMulticast = "238.0.0.1";
	static public int tamanhoMensagem = 1024;
	
	
	 public static String tudo(){
		 String eol = System.getProperty("line.separator");
		return "telaX:"+eol+ x +eol+
			   "telaY:"+eol+y +eol+
			   "dificuldade:"+eol+dificuldade +eol+
			   "velocidadeAsteroide:"+eol+velocidadeAsteroide +eol+
			   "cadenciaArma:"+eol+Heat +eol+
			   "tamanhojogador:"+eol+raioPlayer+eol+
			   "vidaJogador:"+eol+vidaPlayer+eol;
	}
}
